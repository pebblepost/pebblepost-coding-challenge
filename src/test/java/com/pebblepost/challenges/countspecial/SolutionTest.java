/*
 * Copyright (c) 2022 PebblePost Inc.
 * All rights reserved.
 */

package com.pebblepost.challenges.countspecial;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SolutionTest {

    @Test
    void countSpecial_1() {
        assertThat(new Solution().countSpecial(12)).isEqualTo(5);
    }
    
    @Test
    void countSpecial_2() {
        assertThat(new Solution().countSpecial(14)).isEqualTo(5);
    }
    
    @Test
    void countSpecial_3() {
        // Failing test
        // assertThat(new Solution().countSpecial(15)).isEqualTo(6);
    }

}
