# Special Count  
###### PebblePost Engineering Coding Challenge

### Description

* Let `num` be an integer.
* A number is ***"valid"*** if **each of its digits remains a digit after being rotated**.
* We say that `num` is ***"special"*** if after rotating each digit in `num` individually by `π radians or (180 deg)`, we get a ***"valid"*** number that is **not equal to `num`**.

### Problem Statement

* Given an integer `n` complete the logic for `int countSpecial(int n)` that returns the number of `special` integers in the range `[1, n]`.
* Write any additional tests that you think might be required.
* Confirm the code builds (and runs tests) by running `./gradlew build` in the project root.
* See function stub for `int countSpecial(int n)` at:
```
com.pebblepost.challenges.countspecial.Solution.countSpecial:11
```

### Additional Info

* Each digit in `num` must be rotated, **we cannot choose to leave any digit unrotated**.
* 0, 1, and 8 rotate to themselves
* 2 and 5 rotate to each other
* 6 and 9 rotate to each other
* The rest of the numbers do not rotate to any other number and ** are invalid.
* Constraint: `1 <= n <= 10^4`

```
Digit Rotations
---------------
0 -> 0
1 -> 1
2 -> 5
3 -> INVALID DIGIT
4 -> INVALID DIGIT
5 -> 2
6 -> 9
7 -> INVALID DIGIT
8 -> 8
9 -> 6
```

### Examples

```
assertEquals(5, countSpecial(12))

Explanation: There are five special numbers in the range [1, 12] : 2, 5, 6, 9, 12
Note that 1, 3, 4, 7, 10, 11 are not special numbers since they remain unchanged after rotating.
```

```
assertEquals(5, countSpecial(14))

Explanation: There are five special numbers in the range [1, 13] : 2, 5, 6, 9, 12
Note that 1, 3, 4, 7, 10, 11, 13, 14 are not special numbers since they remain unchanged after rotating.
```

```
assertEquals(6, countSpecial(15))

Explanation: There are six special numbers in the range [1, 15] : 2, 5, 6, 9, 12, 15
Note that 1, 3, 4, 7, 10, 11, 13, 14 are not special numbers since they remain unchanged after rotating.
```

See example unit tests at:
```
com.pebblepost.challenges.countspecial.SolutionTest:7
```

### Tooling

* The project uses gradle
* You can build & test the project with Java 11+ on your path by running `./gradlew build`
